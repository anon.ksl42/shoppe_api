﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shopee.Web.API.Models;
using Shopee.Web.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Controllers
{
    [Authorize("AppAuthorize")]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductsController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet]
        public IActionResult GetProduct()
        {
            var result = productService.GetProducts();

            if (result != null)
            {
                var data = result.Select(x => new ShowProduct
                {
                    P_Name = x.P_Name,
                    P_Image = x.P_Image,
                    P_NewPrice = x.P_NewPrice,
                    P_OldPrice = x.P_OldPrice,
                    P_SoldAmount = x.P_SoldAmount
                });
                return Ok(data);
            }
            else
                return NoContent();
        }

        [HttpGet("Detail")]
        public IActionResult GetDetail([FromQuery]int id)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode((int)System.Net.HttpStatusCode.BadRequest, new { errorMessage= "parameter is not valid" });
            }

            var result = productService.GetDetail(id);
            if (result != null)
                return Ok(result);
            else
                return NoContent();
        }
        [HttpGet("Search")]
        public IActionResult Search([FromQuery]string name, int order)
        {
            var result = productService.Search(name, order);
            if (result != null)
                return Ok(result);
            else
                return NoContent();
        }
        
        [HttpGet("Category")]
        public IActionResult GetCategorys()
        {
            var result = productService.GetCategorys();

            if (result != null)
                return Ok(result);
            else
                return NoContent();
        }
    }
}
