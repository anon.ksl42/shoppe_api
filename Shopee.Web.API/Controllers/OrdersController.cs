﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shopee.Web.API.Models;
using Shopee.Web.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Controllers
{
    [Authorize("AppAuthorize")]
    [Route("api/[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService addressService;
        public OrdersController(IOrderService addressService)
        {
            this.addressService = addressService;
        }

        [HttpPost]
        public IActionResult AddAddress([FromBody] OrderModel order)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var res = addressService.AddOder(order);

                if (res != null)
                    return new OkObjectResult(res);
                else
                    return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
