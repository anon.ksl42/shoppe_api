﻿using Microsoft.AspNetCore.Mvc;
using Shopee.Web.API.Models;
using Shopee.Web.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Controllers
{
    [Route("api/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService accountService;
        public AccountsController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        [HttpPost]
        public async Task<IActionResult> AuthLogin([FromBody] CustomerModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var res = await accountService.Login(model);

                if (res != null)
                    return new OkObjectResult(res);
                else
                    return Unauthorized();
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }

            
        }
    }
}
