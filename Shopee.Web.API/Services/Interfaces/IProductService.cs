﻿using Shopee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Services.Interfaces
{
    public interface IProductService
    {
        ProductModel GetDetail(int id);
        List<ProductModel> Search(string name, int order);
        List<CategoryModel> GetCategorys();
        List<ProductModel> GetProducts();

    }
}
