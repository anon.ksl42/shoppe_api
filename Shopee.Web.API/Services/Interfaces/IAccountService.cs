﻿using Shopee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Services.Interfaces
{
    public interface IAccountService
    {
        Task<AuthResponseModel> Login(CustomerModel model);
    }
}
