﻿using Dapper;
using Shopee.Web.API.Models;
using Shopee.Web.API.Repositories.Interfaces;
using Shopee.Web.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Services.Implements
{
    public class OrderService : IOrderService
    {
        private readonly IBaseRepository baseRepository;
        public OrderService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public string AddOder(OrderModel order)
        {
            string phone1 = order.Tal.Substring(0,order.Tal.Length - 9);
            string phone2 = order.Tal.Substring(4,order.Tal.Length - 9);
            string phone3 = order.Tal.Substring(8);

            var phoneDb = phone1 + phone2 + phone3;
            
            var param1 = new DynamicParameters();
            param1.Add("@firstName", order.FirstName);
            param1.Add("@lastName", order.LastName);
            var res1 = baseRepository.QueryStoreProcedure<OrderModel>("Sp_GetIdCustomer", param1).FirstOrDefault();

            
            var param2 = new DynamicParameters();
            param2.Add("@c_id",res1.C_ID);
            param2.Add("@tal", phoneDb);
            param2.Add("@address", order.Address);
            param2.Add("@district", order.District);
            param2.Add("@postcode", order.Postcode);
            param2.Add("@province", order.Province);
            var res2 = baseRepository.ExecuteStoreProcedure<int>("Sp_AddAddress",param2);

            var param3 = new DynamicParameters();
            param3.Add("@c_id", res1.C_ID);
            param3.Add("@p_id", order.P_ID);
            var res3 = baseRepository.ExecuteStoreProcedure<int>("Sp_AddOrder", param3);

            var param4 = new DynamicParameters();
            param4.Add("@p_id", order.P_ID);
            var res4 = baseRepository.QueryStoreProcedure<ProductModel>("Sp_GetAmountandSoldAmount", param4).FirstOrDefault();

            var param5 = new DynamicParameters();
            param5.Add("@p_id", order.P_ID);
            param5.Add("@sold_Amount", res4.P_SoldAmount);
            param5.Add("@amount", res4.P_Amount);
            var res5 = baseRepository.ExecuteStoreProcedure<int>("Sp_SoldAmountandAmount", param5);

            return res2 != 0 && res3 != 0 && res5 != 0 ? "บันทึกข้อมูลการสั่งซื้อเรียบร้อยแล้ว" : "ไม่สามารถบันทึกข้อมูลได้";
        
        }
    }
}
