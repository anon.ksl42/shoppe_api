﻿using Dapper;
using Newtonsoft.Json;
using Shopee.Web.API.Models;
using Shopee.Web.API.Repositories.Interfaces;
using Shopee.Web.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Shopee.Web.API.Services.Implements
{
    public class AccountService : IAccountService
    {
        private readonly IBaseRepository baseRepository;
        public AccountService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public async Task<AuthResponseModel> Login(CustomerModel model)
        {
            var param = new DynamicParameters();
            param.Add("@user",model.Username);
            param.Add("@pass",model.Password);

            var res = baseRepository.QueryStoreProcedure<AuthResponseModel>("Sp_CheckLogin", param).SingleOrDefault();

            if(res == null)
            {
                return null;
            }
            else
            {
                string authServer = "https://x8ona2vu4c.execute-api.ap-southeast-1.amazonaws.com/Prod/api/auth";

                var parameter = new
                {
                    username = model.Username,
                    userPassword = model.Password
                };

                var serailzeObj = JsonConvert.SerializeObject(parameter);

                HttpClient httpClient = new HttpClient();
                StringContent stringContent = new StringContent(serailzeObj, Encoding.UTF8, "application/json");
                HttpResponseMessage responseMessage = await httpClient.PostAsync(authServer, stringContent);

                var responseContent = await responseMessage.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<AuthResponseModel>(responseContent);

                data.C_ID = res.C_ID;
                data.FirstName = res.FirstName;
                data.LastName = res.LastName;

                return data;
            }

        }
    }
}
