﻿using Dapper;
using Shopee.Web.API.Models;
using Shopee.Web.API.Repositories.Interfaces;
using Shopee.Web.API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Services.Implements
{
    public class ProductService : IProductService
    {
        private readonly IBaseRepository baseRepository;
        public ProductService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public List<ProductModel> GetProducts()
        {
            var result = baseRepository.QueryStoreProcedure<ProductModel>("Sp_GetProduct", null).ToList();
            return result;
        }
        public List<CategoryModel> GetCategorys()
        {
            var result = baseRepository.QueryStoreProcedure<CategoryModel>("Sp_GetCatagory", null).ToList();
            return result;
        }

        public ProductModel GetDetail(int id)

        {
            var spParm = new DynamicParameters();
            spParm.Add("@P_ID", id);
            var result = baseRepository.QueryStoreProcedure<ProductModel>("Sp_GetDetail", spParm).FirstOrDefault();
            return result;
        }

        public List<ProductModel> Search(string name, int order)
        {
            if (order == 1)
            {
                var spParm = new DynamicParameters();
                spParm.Add("@name", name);
                var result = baseRepository.QueryStoreProcedure<ProductModel>("Sp_Search", spParm).ToList();
                return result;
            }
            else
            {
                var spParm = new DynamicParameters();
                spParm.Add("@name", name);
                var result = baseRepository.QueryStoreProcedure<ProductModel>("Sp_Search_desc", spParm).ToList();
                return result;
            }

        }
    

    }
}
