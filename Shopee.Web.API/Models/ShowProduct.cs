﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Models
{
    public class ShowProduct
    {
        public string P_Image { get; set; }
        public string P_Name { get; set; }
        public int P_SoldAmount { get; set; }
        public decimal P_OldPrice { get; set; }
        public decimal P_NewPrice { get; set; }
    }
}
