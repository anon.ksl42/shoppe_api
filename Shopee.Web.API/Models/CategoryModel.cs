﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Models
{
    public class CategoryModel
    {
        public int Cate_ID { get; set; }
        public string CateName { get; set; }
        public string CateImage { get; set; }
    }
}
