﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Models
{
    public class ProductModel
    {
        public int P_ID { get; set; }
        public int Cate_ID { get; set; }
        public string P_Name { get; set; }
        public decimal P_OldPrice { get; set; }
        public decimal P_NewPrice { get; set; }
        public string P_Description { get; set; }
        public int P_SoldAmount { get; set; }
        public int P_Amount { get; set; }
        public decimal P_Ratting { get; set; }
        public decimal P_DeliveryPrice { get; set; }
        public string P_Image { get; set; }
    }
}
