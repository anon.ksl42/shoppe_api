﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Models
{
    public class OrderModel
    {
        public int A_ID { get; set; }
        public int C_ID { get; set; }
        public int P_ID { get; set; }

        [Required(ErrorMessage = "กรุณาใส่ FirstName")]
        [MinLength(3, ErrorMessage = "ชื่อต้องมีความยาวอย่างตํ่า 3 ตัวอักษร")]
        [MaxLength(20, ErrorMessage = "ชื่อต้องมีความยาวไม่เกิน 20 ตัวอักษร")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "กรุณาใส่ LastName")]
        [MinLength(3, ErrorMessage = "นามสกุลต้องมีความยาวอย่างตํ่า 3 ตัวอักษร")]
        [MaxLength(20, ErrorMessage = "นามสกุลต้องมีความยาวไม่เกิน 20 ตัวอักษร")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "กรุณาใส่ Tal")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})*-([0-9]{3})*-([0-9]{4})$",ErrorMessage = "กรุณาใส่ เบอร์โทรให้ตรงตาม Format")]
        public string Tal { get; set; }

        [Required(ErrorMessage = "กรุณาใส่ Address")]
        [MaxLength(100, ErrorMessage = "ที่อยู่ต้องมีความยาวไม่เกิน 100 ตัวอักษร")]
        public string Address { get; set; }

        [Required(ErrorMessage = "กรุณาใส่ District")]
        public string District { get; set; }

        [Required(ErrorMessage = "กรุณาใส่ Postcode")]
        [MaxLength(5, ErrorMessage = "รหัสไปรษณีย์มีความยาวไม่เกิน 5 ตัวอักษร")]
        [MinLength(5, ErrorMessage = "รหัสไปรษณีย์ต้องมีความยาว 5 ตัวอักษร")]
        public string Postcode { get; set; }

        [Required(ErrorMessage = "กรุณาใส่ Province")]
        public string Province { get; set; }
    }
}
