﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Models
{
    public class CustomerModel
    {
        public int C_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required(ErrorMessage = "กรุณาใส่ Username")]
        [MinLength(8, ErrorMessage = "ชื่อผู้ใช้ต้องมีความยาวอย่างตํ่า 8 ตัวอักษร")]
        [MaxLength(20, ErrorMessage = "ชื่อผู้ใช้ต้องมีความยาวไม่เกิน 20 ตัวอักษร")]
        public string Username { get; set; }
        [Required(ErrorMessage = "กรุณาใส่ Password")]
        [MinLength(8, ErrorMessage = "รหัสผ่านต้องมีความยาวอย่างตํ่า 8 ตัวอักษร")]
        [MaxLength(20, ErrorMessage = "รหัสผ่านต้องมีความยาวไม่เกิน 20 ตัวอักษร")]
        public string Password { get; set; }
    }
}
