﻿using Dapper;
using Shopee.Web.API.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Repositories.Implements
{
    public class BaseRepository : IBaseRepository
    {
        private readonly string ConnectionString;
        public BaseRepository(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
        }

        private T WithConnection<T>(Func<IDbConnection,T> getData)
        {
            try
            {
                using(var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    return getData(connection);
                }
            }
            catch (SqlException e)
            {

                throw e;
            }
        }

        public int ExecuteStoreProcedure<T>(string spName, DynamicParameters dynamic)
        {
            return WithConnection(x => x.Execute(spName,dynamic, commandType:CommandType.StoredProcedure));
        }

        public IEnumerable<T> QueryStoreProcedure<T>(string spName, DynamicParameters dynamic)
        {
            return WithConnection(x => x.Query<T>(spName, dynamic, commandType:CommandType.StoredProcedure));
        }
    }
}
