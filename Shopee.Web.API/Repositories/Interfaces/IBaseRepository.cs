﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shopee.Web.API.Repositories.Interfaces
{
    public interface IBaseRepository
    {
        IEnumerable<T> QueryStoreProcedure<T>(string spName, DynamicParameters dynamic);

        int ExecuteStoreProcedure<T>(string spName, DynamicParameters dynamic);

    }
}
